import requests

product_id = input("Cual es el id del producto que quieres usar? \n")

try:
    product_id=int(product_id)
except:
    product_id= None
    print(f'{product_id} not a valid id')

if product_id:

    endpoint= f"http://localhost:8000/api/products/{product_id}/delete/"


get_response=requests.delete(endpoint) ## Http Request 

print(get_response.status_code,get_response.status_code==204)
#print(get_response.status_code)